
## Assumptions

1. You have full administrator access to an AWS account

## Overview

There will be two repositories that used to setup the evaluation test

- infra-automation: This repo will store all the cloudformation template related to infrastructure that used to build the deployment pipeline.
                    The developer only need to manage in their own repository. The roll out of the code will be handled by this pipeline
                    https://gitlab.com/tuantai/infra-automation

- code-base: This is the main repo for developer. They will store all the source code and can be used to build the package here.
             In addition, the base template of lambda and api gateway will be stored here.
             https://gitlab.com/tuantai/code-base

## Deployment Pipeline Setup

1. Create IAM User ''PipelineRunner' from AWS Console with programmatic access and attach the Administrator access policy to this user

2. Setup a new project repo (e.g: infra-automation) in Gitlab with 2 branches (develop and master)

3. Configure environment variables with credential in previous step in Gitlab CICD Pipeline Settings

```
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
```
4. Create a Merge request from develop to master to trigger the pipeline

Note: There are default parameters in the template that you want to update with new value

```
  PackageName:
    Description: The name of artifact
    Default: "evaluation.zip"
  BucketName:
    Description: The name of S3 artifact bucket
    Default: "20210709-evaluationtest"
```

5. At this stage, the CodePipeline will be created via the cloudformation. You can check this via AWS Console.
   The pipeline will fail at this stage since we haven't setup the bucket and artifact package.


## Code Base Repo

6. Create a S3 bucket 'ARTIFACT_BUCKET_NAME' (e.g: 20210709-evaluationtest) to store uploaded artifact

7. Create IAM User 'artifactUpload' from AWS Console with programmatic access and attached below policy
(Update BUCKET_NAME from step 1 - e.g: "arn:aws:s3:::20210709-evaluationtest/*" )

````
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:PutObjectAcl"
            ],
            "Resource": [
                "arn:aws:s3:::{ARTIFACT_BUCKET_NAME}/*"
            ],
            "Effect": "Allow"
        }
    ]
}
````

8. Setup a new project repo (e.g: code-base) in Gitlab with 2 branches (develop and master)

9. Configure environment variables with credential in Step 2

```
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
```

10. Create a Merge request from develop to master to trigger the pipeline.
   This will upload the package to S3 and CodePipeline will pick up and start deployment.

11. You can monitor the deployment status from AWS Console Codepipeline and Cloudformation

12. Once the deployment completed, you can test the function via API Gateway (API Key need to be provided)